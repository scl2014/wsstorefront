﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Windows.Controls.Ribbon;

namespace WSStoreFront
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : RibbonWindow
    {
        private Ribbon _ribbon;
        private RibbonToggleButton _toggleButton;
        private RibbonCheckBox _checkBox;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void AddItemCallbackFn(string item)
        {
            fHome.Refresh();
            fReceive.Refresh();
        }

        private void CollapsedAll()
        {
            fHome.Visibility = System.Windows.Visibility.Collapsed;
            fReceive.Visibility = System.Windows.Visibility.Collapsed;
        }

        private void Ribbon_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int item = MainRibbon.SelectedIndex;
            CollapsedAll();
            switch (item)
            {
                case 0:
                    fHome.Visibility = System.Windows.Visibility.Visible;
                    fHome.Refresh();
                    break;
                case 1:
                    fReceive.Visibility = System.Windows.Visibility.Visible;
                    fReceive.Refresh();
                    break;
                default:
                    fHome.Visibility = System.Windows.Visibility.Visible;
                    fHome.Refresh();
                    break;

            }
        }

        private void viewPOs_Click(object sender, RoutedEventArgs e)
        {

        }

        private void PutAwayPO_Click(object sender, RoutedEventArgs e)
        {

        }

        private void ReceivePO_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
